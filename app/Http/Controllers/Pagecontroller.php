<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Slide;
use App\Product;
use App\ProductType;
use App\Cart;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\User;
use App\Contact;
use Hash;
use Auth;
use Session;
class Pagecontroller extends Controller
{
    public function getIndex(){
		$slide= Slide::all();
		$new_product=Product::where('new',1)->paginate(4);
		$promotion_product=Product::where('promotion_price','<>',0)->paginate(12);
		//dd($new_product); 
		//return view('pages.trangchu',['slide'=>$slide]);
		return view('pages.trangchu',compact('slide','new_product','promotion_product'));
	}
	public function getLoaisanpham($type){
		$sp_theoloai= Product::where('id_type',$type)->get();
		$sp_khac= Product::where('id_type','<>',$type)->paginate(3);
		$loai_sp= ProductType::all();
		$l_sp= ProductType::where('id',$type)->first();
		return view('pages.loai_sanpham',compact('sp_theoloai','sp_khac','loai_sp','l_sp'));
	}
	public function getChitietsanpham(Request $req){
		$sanpham= Product::where('id',$req->id)->first();
		$sp_tuongtu= Product::where('id_type',$sanpham->id_type)->paginate(6);
		$sp_moi= Product::where('new',1)->paginate(6);
		$sp_giamgia= Product::where('promotion_price','<>',0)->paginate(6);
		return view('pages.chitiet_sp',compact('sanpham','sp_tuongtu','sp_moi','sp_giamgia'));
	}
	public function getLienhe(){
		return view('pages.contact');
	}
	public function getGioithieu(){
		return view('pages.gioithieu');
	}
	public function getDathang(){
		return view('pages.dathang');
	}
	public function getLogin(){
		return view('pages.login');
	}
	public function getRegister(){
		return view('pages.register');
	}
	public function getAddtocart(Request $req,$id){
		$product= Product::find($id);
		$oldCart= Session('cart') ? Session::get('cart'):null;
		$cart= new Cart($oldCart);
		$cart->add($product,$id);
		$req->session()->put('cart',$cart);
	
		return redirect()->back();
	}
	public function getDelitemcart($id){
		$oldcart = Session::has('cart') ? Session::get('cart'):null;
		$cart= new Cart($oldcart);
		$cart -> removeItem($id);
		if(count($cart->items)>0){
			Session::put('cart',$cart);
		}
		else{
			Session::forget('cart');
		}
		
		return redirect()->back();
	}
    public function postLienhe(Request $req){
        
        $contact = new Contact;

        $contact->your_name = $req->name;
        $contact->your_email = $req->email;
        $contact->subject= $req->subject;
        $contact->your_message = $req->message;

        $contact->save();
         return redirect()->back()->with('thanhcong','Liên hệ thành công');

    }

public function postCheckout(Request $req){
        $cart = Session::get('cart');

        $customer = new Customer;
        $customer->name = $req->name;
        $customer->gender = $req->gender;
        $customer->email = $req->email;
        $customer->address = $req->address;
        $customer->phone_number = $req->phone;
        $customer->note = $req->notes;
        $customer->save();

        $bill = new Bill;
        $bill->id_customer = $customer->id;
        $bill->date_order = date('Y-m-d');
        $bill->total = $cart->totalPrice;
        $bill->payment = $req->payment_method;
        $bill->note = $req->notes;
        $bill->save();

        foreach ($cart->items as $key => $value) {
            $bill_detail = new BillDetail;
            $bill_detail->id_bill = $bill->id;
            $bill_detail->id_product = $key;
            $bill_detail->quantity = $value['qty'];
            $bill_detail->unit_price = ($value['price']/$value['qty']);
            $bill_detail->save();
        }
        Session::forget('cart');
        return redirect()->back()->with('thongbao','Đặt hàng thành công');

    }

    public function postSignup(Request $req){
        $this->validate($req,
            [
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:6|max:20',
                'fullname'=>'required',
                're_password'=>'required|same:password'
            ],
            [
                'email.required'=>'Vui lòng nhập email',
                'email.email'=>'Không đúng định dạng email',
                'email.unique'=>'Email đã có người sử dụng',
                'password.required'=>'Vui lòng nhập mật khẩu',
                're_password.same'=>'Mật khẩu không giống nhau',
                'password.min'=>'Mật khẩu ít nhất 6 kí tự',
                'password.max'=>'Mật khẩu không được quá 20 ký tự'
            ]);
        $user = new User();
        $user->full_name = $req->fullname;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->phone = $req->phone;
        $user->address = $req->address;
        $user->save();
        return redirect()->back()->with('thanhcong','Tạo tài khoản thành công');
    }
    public function postLogin(Request $req){
        $this->validate($req,
            [
                'email'=>'required|email',
                'password'=>'required|min:6|max:20'
            ],
            [
                'email.required'=>'Vui lòng nhập email',
                'email.email'=>'Email không đúng định dạng',
                'password.required'=>'Vui lòng nhập mật khẩu',
                'password.min'=>'Mật khẩu ít nhất 6 kí tự',
                'password.max'=>'Mật khẩu không quá 20 kí tự'
            ]
        );
        $credentials = array('email'=>$req->email,'password'=>$req->password);
        

       
            if(Auth::attempt($credentials)){

            return redirect()->back()->with(['flag'=>'success','message'=>'Đăng nhập thành công']);
            }
            else{
                return redirect()->back()->with(['flag'=>'danger','message'=>'Đăng nhập không thành công']);
            }
     
   
    }
	
	public function postLogout(){
		        Auth::logout();
		        return redirect()->route('trang-chu');
	}

	
	public function getSearch(Request $req){
		$product = Product::where('name','like','%'.$req->key.'%')
							->orWhere('unit_price',$req->key)
							->get();
		return view('pages.search',compact('product'));
	}
}













