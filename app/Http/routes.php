<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('',[
	'as'=>'trang-chu',
	'uses'=>'Pagecontroller@getIndex'
]);
Route::get('loai-san-pham/{type}',[
	'as'=>'loaisanpham',
	'uses'=>'Pagecontroller@getLoaisanpham'
]);
Route::get('chi-tiet-san-pham/{id}',[
	'as'=>'chitietsanpham',
	'uses'=>'Pagecontroller@getChitietsanpham'
]);
Route::get('lien-he',[
	'as'=>'lienhe',
	'uses'=>'Pagecontroller@getLienhe'
]);
Route::get('gioi-thieu',[
	'as'=>'gioithieu',
	'uses'=>'Pagecontroller@getGioithieu'
]);
Route::get('dat-hang',[
	'as'=>'dathang',
	'uses'=>'Pagecontroller@getDathang'
]);
Route::get('dang-nhap',[
	'as'=>'login',
	'uses'=>'Pagecontroller@getLogin'
]);
Route::get('dang-ky',[
	'as'=>'register',
	'uses'=>'Pagecontroller@getRegister'
]);

Route::get('add-to-cart/{id}',[
	'as'=>'themgiohang',
	'uses'=>'Pagecontroller@getAddtocart'
]);
Route::get('del-cart/{id}',[
	'as'=>'xoagiohang',
	'uses'=>'Pagecontroller@getDelitemcart'
]);
Route::post('dat-hang',[
	'as'=>'dathang',
	'uses'=>'Pagecontroller@postCheckout'
]);
Route::post('dang-ki',[
	'as'=>'signup',
	'uses'=>'PageController@postSignup'
]);
Route::post('dang-nhap',[
	'as'=>'login',
	'uses'=>'PageController@postLogin'
]);
Route::get('dang-xuat',[
	'as'=>'logout',
	'uses'=>'PageController@postLogout'
]);
Route::get('search',[
	'as'=>'search',
	'uses'=>'Pagecontroller@getSearch'
]);
Route::post('lien-he',[
	'as'=>'lienhe',
	'uses'=>'Pagecontroller@postLienhe'
]);