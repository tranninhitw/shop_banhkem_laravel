<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table ="products";
	
	public function product_type(){
		return $this-> belongsTo('App\productType','id_type','id');
	}
	public function Bill_detail(){
		return $this-> hasMany('App\BillDetail','id_product','id');
	}
}
